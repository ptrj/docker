# Usage

Environment:
UID, GID

## Start
```
docker-compose pull
docker-compose up --no-start
docker-compose start
```
## Stop
```
docker-compose stop
```
## Upgrade
```
docker-compose stop
docker-compose rm
docker-compose pull
docker-compose up --no-start
docker-compose start
```
