SSL in to snipeit container:
put snipeit-ssl.crt & snipeit-ssl.key in to volumes/snipeit/ssl

# Usage
## Start
```
docker-compose up
```
or
```
docker-compose up --no-start && docker-compose start
```
## Stop
```
docker-compose stop
```

## Upgrade
### Stop
```
docker-compose stop
```
### Remove old container
```
docker-compose rm
```
### Edit and change new image version of snipe-it in docker-compose.yml
```
docker-compose up --no-start && docker-compose start
```
### If databse migration is required
```
docker exec -it $(docker ps | grep snipe/snipe-it | cut -d' ' -f1) /bin/bash
root@ffb95010f7a5:/var/www/html# php artisan migrate
**************************************
*     Application In Production!     *
**************************************

 Do you really wish to run this command? (yes/no) [no]:
 > yes

Migrating: .....
...
exit
```
## Generate API KEY
```
docker pull snipe/snipe-it
docker run --rm -it docker pull snipe/snipe-it php artisan key:generate
Application key [base64:DD0MscREE5+pqdhBjyFQxlE13xenZfnNmEVeDRn6uFA=] set successfully.
```
## Backup
```
docker exec $(docker ps | grep snipe/snipe-it | cut -d' ' -f1) php artisan snipeit:backup
```
## More info
https://snipe-it.readme.io/docs/docker

## Custom "hacks"
After checkout set status label ("2")
```
/app/Models/Asset.php line 223

        if ($name != null) {
            $this->name = $name;
->          $this->status_id = "2";
        }
```