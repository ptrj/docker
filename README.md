# Custom Docker Images
[Docker Compose - Getting Started](https://docs.docker.com/compose/gettingstarted/)

Dokuwiki - https://www.dokuwiki.org/

How Secure Is My Password - https://github.com/howsecureismypassword/hsimp

Network Tools: ab, joomscan, nikto, nmap, siege, sqlmap, whatweb, wpscan

PriveBin - https://privatebin.info/

Snipe-IT - https://snipeitapp.com/

Teampass - https://teampass.net/

Other online network tools: https://securityheaders.com, https://www.ssllabs.com/ssltest/