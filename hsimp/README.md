# How Secure Is My Password
Rather than just saying a password is "weak" or "strong", How Secure is My Password? lets your users know how long it would take 
someone to crack their password. It also checks against the top 10,000 most common passwords as well as a number of other checks 
(such as repeated strings, telephone numbers, and words followed by numbers).

## Start
```
docker-compose build
docker-compose up --no-start
docker-compose start
```
## Stop
```
docker-compose stop
```
## Upgrade
```
docker-compose stop
docker-compose rm
docker-compose build
docker-compose up --no-start
docker-compose start
```

# Links 
https://howsecureismypassword.net/
https://github.com/howsecureismypassword/hsimp
