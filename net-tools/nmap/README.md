# nmap https://nmap.org

## Build
```
docker build -t local/nmap .
```

## Setup
Add in to ~/.bash_profile or ~/.bashrc
```bash
nmap() {
  docker run -it --rm -v /path/to/tmp/nmap:/data local/nmap "$@"
}
```

## Example
```
$ nmap -V
Nmap version 7.70 ( https://nmap.org )
Platform: x86_64-alpine-linux-musl
Compiled with: nmap-liblua-5.3.3 openssl-2.7.2 nmap-libssh2-1.8.0 nmap-libz-1.2.8 nmap-libpcre-7.6 libpcap-1.8.1 nmap-libdnet-1.12 ipv6
Compiled without:
Available nsock engines: epoll poll select
```
```
$ nmap nmap.org
Starting Nmap 7.70 ( https://nmap.org ) at 2018-05-23 13:57 UTC
Nmap scan report for nmap.org (45.33.49.119)
Host is up (0.0036s latency).
Other addresses for nmap.org (not scanned): 2600:3c01::ffff:ffff:fe98:ff4e
rDNS record for 45.33.49.119: ack.nmap.org
Not shown: 992 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
25/tcp   open  smtp
80/tcp   open  http
110/tcp  open  pop3
143/tcp  open  imap
443/tcp  open  https
3128/tcp open  squid-http
8080/tcp open  http-proxy

Nmap done: 1 IP address (1 host up) scanned in 64.68 seconds

# HTTP Auth Brute Password
$ nmap -d -vv -p 443 --script http-brute --script-args http-brute.path=/auth example.com

```
