# Siege https://www.joedog.org/siege-home/

## Build
```
docker build -t local/siege .
```

## Setup
Add in to ~/.bash_profile or ~/bashrc
```bash
siege() {
  docker run -it --rm -v /path/to/tmp/siege:/data local/siege "$@"
}
```
## Example
siege -A "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0" www.example.com
