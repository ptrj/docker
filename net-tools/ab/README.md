# Apache Benchmarking Tool https://httpd.apache.org/docs/2.4/programs/ab.html

## Build
```
docker build -t local/ab .
```

## Setup
Add in to ~/.bash_profile or ~/bashrc
```bash
ab() {
  docker run -it --rm -v /path/to/tmp/ab:/data local/ab "$@"
}
```
## Example
```
ab -n 100 -c 10 -l http://www.example.com
```
