# nikto https://www.cirt.net/Nikto2

## Build
```
docker build -t local/nikto .
```

## Setup
Add in to ~/.bash_profile or ~/.bashrc
```bash
nikto() {
  docker run -it --rm -v /path/to/tmp/nikto:/data local/nikto "$@"
}
```

## Example

```bash
nikto() {
  docker run -it --rm -v /path/to/tmp/nikto:/data local/nikto -useragent "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0" "$@"
}
```
```
$ nitko -host http://example.com
```

## Read more
https://tools.kali.org/information-gathering/nikto
