# sqlmap http://sqlmap.org/

## Build
```
docker build -t local/sqlmap .
```
## Setup
Add in to ~/.bash_profile or ~/.bashrc
```bash
sqlmap() {
  docker run -it --rm -v /path/to/tmp/sqlmap:/root/.sqlmap local/sqlmap --random-agent "$@"
}
```

## Read more
https://www.owasp.org/index.php/SQL_Injection
