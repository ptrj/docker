# JoomScan https://github.com/rezasp/joomscan

## Build
```
docker build -t local/joomscan .
```

## Setup
Add in to ~/.bash_profile or ~/bashrc
```bash
joomscan() {
  docker run -it --rm -v /path/to/tmp/joomscan:/data local/joomscan -a "Googlebot/2.1 (+http://www.googlebot.com/bot.html)" "$@"
}
```
## Example
https://github.com/rezasp/joomscan
