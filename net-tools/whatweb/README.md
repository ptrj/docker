# WhatWeb https://github.com/urbanadventurer/WhatWeb

## Build
```
docker pull guidelacour/whatweb
```

## Setup
Add in to ~/.bash_profile or ~/.bashrc
```bash
whatweb() {
  docker run -it --rm guidelacour/whatweb ./whatweb -U="Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0" "$@"
}
```

## Example
```
$ whatweb github.com
http://github.com [301 Moved Permanently] Country[UNITED STATES][US], IP[192.30.253.112], RedirectLocation[https://github.com/]
https://github.com/ [200 OK] Cookies[_gh_sess,logged_in], Country[UNITED STATES][US], Email[you@example.com], HTML5, HTTPServer[GitHub.com], HttpOnly[_gh_sess,logged_in], IP[192.30.253.112], Open-Graph-Protocol[1401488693436528], OpenSearch[/opensearch.xml], PasswordField[user[password]], Script[application/javascript], Strict-Transport-Security[max-age=31536000; includeSubdomains; preload], Title[The world’s leading software development platform · GitHub], UncommonHeaders[x-request-id,x-content-type-options,referrer-policy,expect-ct,content-security-policy,x-runtime-rack,x-github-request-id], X-Frame-Options[deny], X-XSS-Protection[1; mode=block]
```
