# WPScan https://github.com/wpscanteam/wpscan

## Build
```
docker pull wpscanteam/wpscan
```

## Setup
Add in to ~/.bash_profile or ~/bashrc
```bash
wpscan() {
  docker run -it --rm wpscanteam/wpscan "$@"
}
```
## Example
```bash
wpscan() {
  docker run -it --rm wpscanteam/wpscan -a "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0" -u "$@"
}
```
```
$ wpscan wordpress.com
_______________________________________________________________
        __          _______   _____
        \ \        / /  __ \ / ____|
         \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
          \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
           \  /\  /  | |     ____) | (__| (_| | | | |
            \/  \/   |_|    |_____/ \___|\__,_|_| |_|

        WordPress Security Scanner by the WPScan Team
                       Version 2.9.4-dev
          Sponsored by Sucuri - https://sucuri.net
      @_WPScan_, @ethicalhack3r, @erwan_lr, @_FireFart_
_______________________________________________________________

[i] The remote host tried to redirect to: https://wordpress.com/
[?] Do you want follow the redirection ? [Y]es [N]o [A]bort, default: [N] >y
[+] URL: https://wordpress.com/
[+] Started: Wed May 23 13:02:43 2018

[+] Interesting header: LINK: 
<https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese>; 
rel=preload; as=style
[+] Interesting header: LINK: 
<https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i&subset=cyrillic,cyrillic-ext,devanagari,greek,greek-ext,latin-ext,vietnamese>; 
rel=preload; as=style
[+] Interesting header: LINK: <https://s1.wp.com/home.logged-out/page-domain-only/img/hero-icons.svg>; rel=image; as=style
[+] Interesting header: SERVER: nginx
[+] Interesting header: STRICT-TRANSPORT-SECURITY: max-age=15552000; preload
[+] Interesting header: X-AC: 1.vie _dca
[+] Interesting header: X-FRAME-OPTIONS: SAMEORIGIN
[+] robots.txt available under: https://wordpress.com/robots.txt   [HTTP 200]
[+] Interesting entry from robots.txt: /activate/ # har har   [HTTP 0]
[+] Interesting entry from robots.txt: /cgi-bin/ # MT refugees   [HTTP 0]
[+] Interesting entry from robots.txt: https://wordpress.com/mshots/v1/   [HTTP 404]
[+] Interesting entry from robots.txt: https://wordpress.com/next/   [HTTP 200]
[+] Interesting entry from robots.txt: https://wordpress.com/public.api/   [HTTP 403]
[+] Sitemap found: https://wordpress.com/robots.txt   [HTTP 200]
[+] Sitemap entry: https://wordpress.com/sitemap.xml   [HTTP 200]
[+] This site has 'Must Use Plugins' (http://codex.wordpress.org/Must_Use_Plugins)
[+] XML-RPC Interface available under: https://wordpress.com/xmlrpc.php   [HTTP 405]
[+] Found an RSS Feed: //en.blog.wordpress.com/feed/   [HTTP 0]

[+] Enumerating WordPress version ...

[+] WordPress version 4.9.6 (Released on 2018-05-17) identified from advanced fingerprinting

[+] Enumerating plugins from passive detection ...
[+] No plugins found passively

[+] Finished: Wed May 23 13:02:50 2018
[+] Elapsed time: 00:00:07
[+] Requests made: 55
[+] Memory used: 48.699 MB
```
