# Install
```
docker build -t local/sphinx .
```

# First run
```
docker run -d -v $(pwd)/web:/web -p 8000:8000 --name sphinx-server local/sphinx
docker exec -it sphinx-server sphinx-quickstart
```
![Quick start](sphinx-configure.png)

# Second run or Existing documentation
```
docker run -d -v $(pwd)/web:/web -p 8000:8000 --name sphinx-server local/sphinx
```

# Change theme
Edit $(pwd)/web/conf.py
```
import sphinx_rtd_theme
html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
```

# Other
```
docker exec -it sphinx-server /bin/sh
docker container restart sphinx-server
```

# Links
https://medium.com/@richyap13/a-simple-tutorial-on-how-to-document-your-python-project-using-sphinx-and-rinohtype-177c22a15b5b  
https://www.ibm.com/developerworks/library/os-sphinx-documentation/ 

# From
https://hub.docker.com/r/dldl/sphinx-server/  
https://github.com/pardahlman/docker-sphinx
