#!/bin/bash

if [ $# -gt 0 ];then
  exec "$@"
else
  /usr/sbin/apache2ctl -X
fi
