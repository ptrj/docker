# PHP Apache
Is Apache + mod_php universal container for your PHP WebApp  
Apache 2.4.25-3+deb9u6  
PHP 5.6 (you can change it to PHP 7.x in php-apache/Dockerfile)

# Usage
## Start
```
docker-compose build
docker-compose up --no-start
docker-compose start
```
## Stop
```
docker-compose stop
```
## Upgrade
```
docker-compose stop
docker-compose rm
docker-compose build
docker-compose up --no-start
docker-compose start
```
