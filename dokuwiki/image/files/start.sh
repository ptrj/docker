#!/bin/bash

if [ ! -f /var/log/firstrun ]
then
    test "$(ls -A /var/www/dokuwiki/conf | grep -v .empty)" || cp -a /var/www/dokuwiki/conf_copy/. /var/www/dokuwiki/conf
    test "$(ls -A /var/www/dokuwiki/data | grep -v .empty)" || cp -a /var/www/dokuwiki/data_copy/. /var/www/dokuwiki/data
    test "$(ls -A /var/www/dokuwiki/lib/plugins | grep -v .empty)" || cp -a /var/www/dokuwiki/lib/plugins_copy/. /var/www/dokuwiki/lib/plugins
    test "$(ls -A /var/www/dokuwiki/lib/tpl | grep -v .empty)" || cp -a /var/www/dokuwiki/lib/tpl_copy/. /var/www/dokuwiki/lib/tpl
    test "$(ls -A /var/log | grep -v .empty)" || cp -a /var/log_copy/. /var/log
    touch /var/log/firstrun
fi

mkfifo -m 600 /tmp/logpipe
cat <> /tmp/logpipe 1>&2 &
chown www-data /tmp/logpipe
/usr/sbin/lighttpd -D -f /etc/lighttpd/lighttpd.conf
