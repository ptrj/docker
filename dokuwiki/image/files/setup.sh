#!/bin/bash

# Set PHP
sed -i 's/;date.timezone =/date.timezone = '${CONTINENT}'\/'${CITY}'/' /etc/php/7.2/cgi/php.ini && \
sed -i 's/memory_limit = .*/memory_limit = 256M/' /etc/php/7.2/cgi/php.ini && \
sed -i 's/post_max_size = .*/post_max_size = 256M/' /etc/php/7.2/cgi/php.ini && \
sed -i 's/upload_max_filesize = .*/upload_max_filesize = 256M/' /etc/php/7.2/cgi/php.ini && \
# Set Lighttpd
sed -i 's/server.errorlog .*/server.errorlog             = "\/tmp\/logpipe"/' /etc/lighttpd/lighttpd.conf && \
sed -i 's/accesslog.filename .*/accesslog.filename = "\/tmp\/logpipe"/' /etc/lighttpd/conf-available/10-accesslog.conf && \
# Set Locale
locale-gen en_US.UTF-8 && update-locale LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8 && \
# Seti Timezone
rm -rf /etc/localtime && cp /usr/share/zoneinfo/${CONTINENT}/${CITY} /etc/localtime && \
cp -a /var/www/dokuwiki/conf /var/www/dokuwiki/conf_copy && \
cp -a /var/www/dokuwiki/data /var/www/dokuwiki/data_copy && \
cp -a /var/www/dokuwiki/lib/plugins /var/www/dokuwiki/lib/plugins_copy && \
cp -a /var/www/dokuwiki/lib/tpl /var/www/dokuwiki/lib/tpl_copy && \
cp -a /var/log /var/log_copy
