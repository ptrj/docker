# Usage
## Start
```
docker-compose build
docker-compose up -d
```
## Stop
```
docker-compose stop
```
## Upgrade
```
docker-compose stop
docker-compose rm
 - Change Version & Checksum in docker-compose.yaml
docker-compose build
docker-compose up -d
```
## Known Issues
```
dokuwiki_1  | 127.0.0.1 localhost - [06/Oct/2020:13:26:03 +0200] "HEAD /doku.php HTTP/1.1" 500 0 "-" "-"
dokuwiki_1  | 2020-10-06 13:27:04: (mod_fastcgi.c.2543) FastCGI-stderr: PHP Fatal error:  Class 'dokuwiki\plugin\config\core\Setting\Setting' not found in /var/www/dokuwiki/inc/deprecated.php on line 61
```
Backup the Plugins directory and replace the content with the original content from the installation
